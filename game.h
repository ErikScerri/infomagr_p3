#pragma once

namespace Tmpl8 {

class Game
{
public:
	void SetTarget( Surface* surface ) { screen = surface; }
	void Init();
	void Shutdown();
	void Tick( float deltaTime );
	bool HandleInput();
	void MouseUp( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseDown( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseMove( int x, int y ) { /* implement if you want to detect mouse movement */ }
	void KeyUp( int key ) { /* implement if you want to handle keys */ }
	void KeyDown( int key ) { /* implement if you want to handle keys */ }
	void AddObject(const char* path, Material mat, vec3 offset);
	void ParseScene();
private:
	Surface* screen;
	Surface* curr;
	Renderer renderer;

	ObjMesh objmesh;

	std::vector<tinyobj::attrib_t> scene_attribs;
	std::vector<tinyobj::shape_t> scene_shapes;
	std::vector<Material> scene_mats;
	std::vector<vec3> scene_offsets;

	std::vector<std::unique_ptr<Object>> objects;
	std::vector<std::unique_ptr<Light>> lights;
	std::vector<std::unique_ptr<BVH>> bvhs;
	Camera camera = Camera(90, vec3(0, 0, 0), vec3(0, 0, 0));

};

}; // namespace Tmpl8