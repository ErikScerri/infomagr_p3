#define TINYOBJLOADER_IMPLEMENTATION

#include "precomp.h" // include (only) this in every .cpp file

#define USE_BVH

//std::default_random_engine generator;

inline float deg2rad(const float &deg) { return deg * PI / 180; }

inline Pixel clr2pix(const Color &color)
{
	const unsigned r = (uint32_t)(255 * CLAMP(color.x, 0, 1)) << 16;
	const unsigned g = (uint32_t)(255 * CLAMP(color.y, 0, 1)) << 8;
	const unsigned b = (uint32_t)(255 * CLAMP(color.z, 0, 1));

	return r + g + b;
}

inline float xor128()
{
	static uint32_t x = 123456789;
	static uint32_t y = 362436069;
	static uint32_t z = 521288629;
	static uint32_t w = 88675123;
	uint32_t t;
	t = x ^ (x << 11);
	x = y; y = z; z = w;
	w = w ^ (w >> 19) ^ (t ^ (t >> 8));
	return w / 4294967295.0f;
}

mat4 Renderer::SetCameraMatrix(const Camera &camera)
{
	mat4 cameraToWorld = mat4();

	cameraToWorld = cameraToWorld.rotatexyz(camera.orient.x, camera.orient.y, camera.orient.z);

	vec3 dirx = transformvector(vec3(1, 0, 0), cameraToWorld);
	vec3 diry = transformvector(vec3(0, 1, 0), cameraToWorld);
	vec3 dirz = transformvector(vec3(0, 0, 1), cameraToWorld);

	vec3 dir = (dirx * camera.pos.x) + (diry * camera.pos.y) + (dirz * camera.pos.z);

	cameraToWorld.cell[3] = dir.x;
	cameraToWorld.cell[7] = dir.y;
	cameraToWorld.cell[11] = dir.z;

	return cameraToWorld;
}

void Renderer::Render(const Camera &camera, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, Surface* screen)
{
	mat4 cameraToWorld = SetCameraMatrix(camera);

	float scale = tan(deg2rad(camera.fov * 0.5));
	float aspectratio = camera.w / (float)camera.h;

	vec3 orig = transformpoint(vec3(0), cameraToWorld);

	clock_t start, end;
	start = clock();
		
	Color* tmpbuffer = (Color*)MALLOC64(SCRWIDTH * SCRHEIGHT * sizeof(Color), 64);

	for (int j = 0; j < camera.h; j++) for (int i = 0; i < camera.w; i++)
	{
		/*if (i == 395 && j == 390)
		{
			int test = 0;
		}*/
		
		float x = (2 * (i + 0.5) / (float)camera.w - 1) * aspectratio * scale;
		float y = (1 - 2 * (j + 0.5) / (float)camera.h) * scale;

		vec3 dir = transformvector(vec3(x, y, -1), cameraToWorld);
		dir.normalize();

		Ray ray = Ray(orig, dir, PRIMARY);
		Color result = Sample(ray, objects, lights, bvhs, camera, 0, false);

		if (!firstflag)
		{
			result += buffer[i + j * SCRWIDTH];
			result *= 0.5f;
		}

		buffer[i + j * SCRWIDTH] = result;

		//tmpscreen->Plot(i, j, clr2pix(result));
		//screen->Plot(i, j, clr2pix(result));
	}

	pp_BOX_FILTER(buffer, tmpbuffer, SCRWIDTH, SCRHEIGHT, 1);
	//pp_BLUR_LIGHT_FILTER(buffer, tmpbuffer, SCRWIDTH, SCRHEIGHT);
	//pp_BLUR_HEAVY_FILTER(buffer, tmpbuffer, SCRWIDTH, SCRHEIGHT);

	for (int j = 0; j < camera.h; j++) for (int i = 0; i < camera.w; i++)
	{
		screen->Plot(i, j, clr2pix(buffer[i + j * SCRWIDTH]));
	}

	if (firstflag) { firstflag = false; }

	end = clock();

	//std::cout << "Render complete: " << (double)(end - start) / CLOCKS_PER_SEC << " seconds" << std::endl;
}

void Renderer::pp_BOX_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height, const unsigned int radius)
{
	const float scale = 1.0f / (float)((radius << 1) + 1);

	// Do left edge
	Color t = src[0] * radius;
	for (unsigned int x = 0; x < (radius + 1); ++x)
		t += src[x];
	dst[0] = t * scale;

	for (unsigned int x = 1; x < (radius + 1); ++x) {
		t += src[x + radius];
		t -= src[0];
		dst[x] = t * scale;
	}

	// Main loop
	for (unsigned int x = (radius + 1); x < width - radius; ++x) {
		t += src[x + radius];
		t -= src[x - radius - 1];
		dst[x] = t * scale;
	}

	// Do right edge
	for (unsigned int x = width - radius; x < width; ++x) {
		t += src[width - 1];
		t -= src[x - radius - 1];
		dst[x] = t * scale;
	}
}

void Renderer::pp_BOX_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height, const unsigned int radius)
{
	const float scale = 1.0f / (float)((radius << 1) + 1);

	// Do left edge
	Color t = src[0] * radius;
	for (unsigned int y = 0; y < (radius + 1); ++y) {
		t += src[y * width];
	}
	dst[0] = t * scale;

	for (unsigned int y = 1; y < (radius + 1); ++y) {
		t += src[(y + radius) * width];
		t -= src[0];
		dst[y * width] = t * scale;
	}

	// Main loop
	for (unsigned int y = (radius + 1); y < (height - radius); ++y) {
		t += src[(y + radius) * width];
		t -= src[((y - radius) * width) - width];
		dst[y * width] = t * scale;
	}

	// Do right edge
	for (unsigned int y = height - radius; y < height; ++y) {
		t += src[(height - 1) * width];
		t -= src[((y - radius) * width) - width];
		dst[y * width] = t * scale;
	}
}

void Renderer::pp_BOX_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height, const unsigned int radius)
{
	for (unsigned int i = 0; i < height; ++i)
		pp_BOX_FILTER_x(&frameBuffer[i * width], &tmpFrameBuffer[i * width], width, height, radius);

	for (unsigned int i = 0; i < width; ++i)
		pp_BOX_FILTER_y(&tmpFrameBuffer[i], &frameBuffer[i], width, height, radius);
}

void Renderer::pp_BLUR_LIGHT_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height)
{
	const float aF = .15f;
	const float bF = 1.f;
	const float cF = .15f;

	// Do left edge
	Color a = 0.f;
	Color b = src[0];
	Color c = src[1];

	const float leftTotF = bF + cF;
	const float bLeftK = bF / leftTotF;
	const float cLeftK = cF / leftTotF;
	dst[0] = bLeftK  * b + cLeftK * c;

	// Main loop
	const float totF = aF + bF + cF;
	const float aK = aF / totF;
	const float bK = bF / totF;
	const float cK = cF / totF;

	for (unsigned int x = 1; x < width - 1; ++x) {
		a = b;
		b = c;
		c = src[x + 1];

		dst[x] = aK * a + bK * b + cK * c;
	}

	// Do right edge
	const float rightTotF = aF + bF;
	const float aRightK = aF / rightTotF;
	const float bRightK = bF / rightTotF;
	a = b;
	b = c;
	dst[width - 1] = aRightK * a + bRightK * b;
}

void Renderer::pp_BLUR_LIGHT_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height)
{
	const float aF = .15f;
	const float bF = 1.f;
	const float cF = .15f;

	// Do left edge
	Color a = 0.f;
	Color b = src[0];
	Color c = src[width];

	const float leftTotF = bF + cF;
	const float bLeftK = bF / leftTotF;
	const float cLeftK = cF / leftTotF;
	dst[0] = bLeftK  * b + cLeftK * c;

	// Main loop
	const float totF = aF + bF + cF;
	const float aK = aF / totF;
	const float bK = bF / totF;
	const float cK = cF / totF;

	for (unsigned int y = 1; y < height - 1; ++y) {
		a = b;
		b = c;
		c = src[(y + 1) * width];

		dst[y * width] = aK * a + bK * b + cK * c;
	}

	// Do right edge
	const float rightTotF = aF + bF;
	const float aRightK = aF / rightTotF;
	const float bRightK = bF / rightTotF;
	a = b;
	b = c;
	dst[(height - 1) * width] = aRightK * a + bRightK * b;
}

void Renderer::pp_BLUR_LIGHT_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height)
{
	for (unsigned int i = 0; i < height; ++i)
		pp_BLUR_LIGHT_FILTER_x(&frameBuffer[i * width], &tmpFrameBuffer[i * width], width, height);

	for (unsigned int i = 0; i < width; ++i)
		pp_BLUR_LIGHT_FILTER_y(&tmpFrameBuffer[i], &frameBuffer[i], width, height);
}

void Renderer::pp_BLUR_HEAVY_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height)
{
	const float aF = .35f;
	const float bF = 1.f;
	const float cF = .35f;

	// Do left edge
	Color a = 0.f;
	Color b = src[0];
	Color c = src[1];

	const float leftTotF = bF + cF;
	const float bLeftK = bF / leftTotF;
	const float cLeftK = cF / leftTotF;
	dst[0] = bLeftK  * b + cLeftK * c;

	// Main loop
	const float totF = aF + bF + cF;
	const float aK = aF / totF;
	const float bK = bF / totF;
	const float cK = cF / totF;

	for (unsigned int x = 1; x < width - 1; ++x) {
		a = b;
		b = c;
		c = src[x + 1];

		dst[x] = aK * a + bK * b + cK * c;
	}

	// Do right edge
	const float rightTotF = aF + bF;
	const float aRightK = aF / rightTotF;
	const float bRightK = bF / rightTotF;
	a = b;
	b = c;
	dst[width - 1] = aRightK * a + bRightK * b;
}

void Renderer::pp_BLUR_HEAVY_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height)
{
	const float aF = .35f;
	const float bF = 1.f;
	const float cF = .35f;

	// Do left edge
	Color a = 0.f;
	Color b = src[0];
	Color c = src[width];

	const float leftTotF = bF + cF;
	const float bLeftK = bF / leftTotF;
	const float cLeftK = cF / leftTotF;
	dst[0] = bLeftK  * b + cLeftK * c;

	// Main loop
	const float totF = aF + bF + cF;
	const float aK = aF / totF;
	const float bK = bF / totF;
	const float cK = cF / totF;

	for (unsigned int y = 1; y < height - 1; ++y) {
		a = b;
		b = c;
		c = src[(y + 1) * width];

		dst[y * width] = aK * a + bK * b + cK * c;
	}

	// Do right edge
	const float rightTotF = aF + bF;
	const float aRightK = aF / rightTotF;
	const float bRightK = bF / rightTotF;
	a = b;
	b = c;
	dst[(height - 1) * width] = aRightK * a + bRightK * b;
}

void Renderer::pp_BLUR_HEAVY_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height)
{
	for (unsigned int i = 0; i < height; ++i)
		pp_BLUR_HEAVY_FILTER_x(&frameBuffer[i * width], &tmpFrameBuffer[i * width], width, height);

	for (unsigned int i = 0; i < width; ++i)
		pp_BLUR_HEAVY_FILTER_y(&tmpFrameBuffer[i], &frameBuffer[i], width, height);
}

Color Renderer::Sample(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, const Camera &camera, uint32_t depth, const bool lastSpecular)
{
	if (depth > MAX_DEPTH) return camera.background;

	Color hitColor = 0;
	//uint32_t collidx = 0;

	//Inverse ray for Bounding Box intersection (pre-calculated to avoid multiple calculations)
	vec3 invdir = vec3(1 / ray.dir.x, 1 / ray.dir.y, 1 / ray.dir.z);
	int sign[3] = { invdir.x < 0, invdir.y < 0, invdir.z < 0 };
	Ray invray = Ray(ray.orig, invdir, ray.type);

	//Determines if previous BVH has resulted in an intersection already
	bool prevcoll = false;

	//Same process of intersection but goes through all BVHs
	for (int i = 0; i < bvhs.size(); i++)
	{
		BVH* currBVH = bvhs[i].get();
		Object* hitObj = objects[currBVH->objidx].get();
		uint32_t collidx = 0;

		if (TraverseBVH(ray, invray, sign, currBVH, currBVH->root, hitObj, collidx))
		{
			prevcoll = true;

			Material hitMat = hitObj->GetMaterial(collidx);

			vec3 hitPoint = ray.orig + ray.dir * ray.t;
			vec3 hitNorm;
			hitObj->GetSurfaceData(hitPoint, hitNorm, collidx);

			vec3 BRDF = (hitMat.color * hitMat.albedo) * (1 / PI);

			if (hitMat.type == LIGHT)
			{
				if (lastSpecular || depth == 0) hitColor = hitMat.color;
				else hitColor = BLACK;
				continue;
			}

			int randLight = rand() % lights.size();

			vec3 lightOrig, lightNorm;
			float lightArea;
			Light* currLight = lights[randLight].get();

			currLight->RandomPointOnLight(lightOrig, lightNorm, lightArea, generator);
			vec3 lightDir = lightOrig - hitPoint;
			float dist = lightDir.length();
			Ray lightRay = Ray(hitPoint + hitNorm * SHADOWBIAS, lightDir, SHADOW);
			lightRay.t = dist;

			//Inverse ray for Bounding Box intersection (pre-calculated to avoid multiple calculations)
			vec3 invlightDir = vec3(1 / lightDir.x, 1 / lightDir.y, 1 / lightDir.z);
			int lightsign[3] = { invlightDir.x < 0, invlightDir.y < 0, invlightDir.z < 0 };
			Ray invlightRay = Ray(lightOrig, invlightDir, SHADOW);				

			bool blocked = false;
			for (int j = 0; j < bvhs.size(); j++)
			{
				BVH* currBVH = bvhs[j].get();
				Object* hitObj = objects[currBVH->objidx].get();
				uint32_t lightcollidx = 0;

				blocked |= TraverseBVH(lightRay, invlightRay, lightsign, currBVH, currBVH->root, hitObj, lightcollidx);
			}

			Color lightColor = BLACK;
			if (dot(hitNorm, lightDir) > 0 && dot(lightNorm, -lightDir) > 0) if (!blocked)
			{
				float solidAngle = ((dot(lightNorm, -lightDir)) * lightArea) / (dist * dist);
				lightColor = currLight->color * currLight->intensity * solidAngle * BRDF * dot(hitNorm, lightDir);
			}

			if (hitNorm.x == 1)
			{
				int test = 0;
			}

			vec3 reflOrig = hitPoint + hitNorm * SHADOWBIAS;
			vec3 reflDir;
			switch (hitMat.type)
			{
				case DIFFUSE:
				{
					float r1 = xor128();
					float r2 = xor128();

					reflDir = Diffuse(hitNorm, r1, r2);

					Ray reflRay = Ray(reflOrig, reflDir);

					float pdf = dot(hitNorm, reflDir) / PI;

					Color Ei = Sample(reflRay, objects, lights, bvhs, camera, depth + 1, false) * (dot(hitNorm, reflDir) / pdf);
					hitColor = /*PI * 2.0f **/ BRDF * Ei;
					break;
				}
				case MIRROR:
				{
					reflDir = Reflect(ray.dir, hitNorm);
					Ray reflRay = Ray(reflOrig, reflDir);
					hitColor = 0.8 * hitMat.albedo * Sample(reflRay, objects, lights, bvhs, camera, depth + 1, true);
					break;
				}
				case DIELECTRIC:
				{
					float kr;
					Fresnel(ray.dir, hitNorm, hitMat.idxofref, kr);

					bool outside = dot(ray.dir, hitNorm) < 0;
					vec3 bias = SHADOWBIAS * hitNorm;

					float choice = xor128();

					if (choice >= kr && kr < 1)
					{
						vec3 refrDir = Refract(ray.dir, hitNorm, hitMat.idxofref).normalized();
						vec3 refrOrig = outside ? hitPoint - bias : hitPoint + bias;
						Ray refrRay = Ray(refrOrig, refrDir);
						hitColor = Sample(refrRay, objects, lights, bvhs, camera, depth + 1, false);
					}
					else
					{
						vec3 reflDir = Reflect(ray.dir, hitNorm).normalized();
						vec3 reflOrig = outside ? hitPoint + bias : hitPoint - bias;
						Ray reflRay = Ray(reflOrig, reflDir);
						hitColor = BRDF * Sample(reflRay, objects, lights, bvhs, camera, depth + 1, false);
					}
					break;
				}
				default:
					break;
			}

			hitColor += lightColor;
		}
		else
		{
			if (!prevcoll) hitColor = camera.background;
		}
	}

	return hitColor;
}

bool Renderer::TraverseBVH(Ray &ray, Ray &invray, int* sign, BVH* bvh, BVHNode &node, Object* &hitObj, uint32_t &collidx)
{
	//If the biggest bounding box doesn't intersect, no smaller ones will
	if (!node.bbox.Intersect(invray, sign)) return false;

	//If the number of primitives is less than the minimum number, node is a leaf
	if (node.count < LEAFCOUNT)
	{
		return NearestIntersection(ray, hitObj, node.left_first, node.count, collidx);
	}
	else
	{
		//Determine which side of the tree (left or right) is closest to ray origin, traverse that first
		vec3 vecToLeft = ray.orig - bvh->pool[node.left_first].bbox.Centroid().tovec3();
		vec3 vecToRight = ray.orig - bvh->pool[node.left_first + 1].bbox.Centroid().tovec3();

		float distToLeft = vecToLeft.sqrLength();
		float distToRight = vecToRight.sqrLength();

		if (distToLeft < distToRight)
		{
			bool left = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first], hitObj, collidx);
			bool right = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first + 1], hitObj, collidx);
			return left || right;
		}
		else
		{
			bool left = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first + 1], hitObj, collidx);
			bool right = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first], hitObj, collidx);
			return left || right;
		}

	}
}

bool Renderer::NearestIntersection(Ray &ray, Object* &hitObj, const int &first, const int &count, uint32_t &collidx)
{
	if (ray.type == SHADOW && hitObj->GetMaterial(collidx).type == DIELECTRIC) return false;
	if (ray.type == SHADOW && hitObj->GetMaterial(collidx).type == LIGHT) return false;
	Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
	uint32_t collidxTMP = 0;
	if (hitObj->Intersect(rayTMP, collidxTMP, first, count) && rayTMP.t < ray.t)
	{
		ray.t = rayTMP.t;
		collidx = collidxTMP;
		return true;
	}
	return false;
}

bool Renderer::NearestIntersection(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const Object* &hitObj, uint32_t &collidx)
{	
	for (int i = 0; i < objects.size(); i++)
	{
		if (ray.type == SHADOW && objects[i]->GetMaterial(collidx).type == DIELECTRIC) continue;
		Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
		uint32_t collidxTMP = 0;
		if (objects[i]->Intersect(rayTMP, collidxTMP) && rayTMP.t < ray.t)
		{
			hitObj = objects[i].get();
			ray.t = rayTMP.t;
			collidx = collidxTMP;
		}			
	}

	return (hitObj != nullptr);
}

bool Renderer::LoadObjFile(const char* objfile, const char* mtlfile, bool triangulate, ObjMesh &obj)
{
	std::cout << "Loading " << objfile << "..." << std::endl;

	std::string err;
	bool ret = tinyobj::LoadObj(&obj.attrib, &obj.shapes, &obj.materials, &err, objfile, mtlfile, triangulate);

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
		return false;
	}

	return true;
}