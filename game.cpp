#include "precomp.h" // include (only) this in every .cpp file

#define DEBUG_INFO
#define STEP_SIZE_POS 1
#define STEP_SIZE_FOV 1
#define STEP_SIZE_DEG 5
#define STEP_SIZE_ORI (STEP_SIZE_DEG * M_PI / 180)

uint numtri = 0;

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
	curr = new Surface(SCRWIDTH, SCRHEIGHT);

	// -----------------------------------------------------------
	// OBJ Meshes:
	// --- Load file into ObjMesh using tiny_obj_loader
	// --- Create TriangleMesh object from loaded ObjMesh, using any Material definition
	// --- Add offset parameter at end of constructor (see cube) to move
	// -----------------------------------------------------------

	AddObject("objects/cube.obj", Material(vec3(0.1, 0.8, 0.1), DIFFUSE), vec3(1, 0, 0));
	AddObject("objects/cube.obj", Material(vec3(0.5, 0.1, 0.1), DIELECTRIC), vec3(-1, 0, 0));
	AddObject("objects/plane.obj", Material(vec3(0.2, 0.2, 1), DIFFUSE), vec3(0, -0.5, 0));
	AddObject("objects/wall_back.obj", Material(vec3(0.5, 0.5, 0.5), MIRROR), vec3(0, -0.5, -3));

	ParseScene();

	//renderer.LoadObjFile("objects/teapot_full.obj", NULL, true, objmesh); //-150 z, -20 y
	//renderer.LoadObjFile("objects/tree.obj", NULL, true, objmesh); //-30 z, -20 y
	//renderer.LoadObjFile("objects/cube.obj", NULL, true, objmesh);

	renderer.LoadObjFile("objects/square.obj", NULL, true, objmesh);
	for (int i = 0; i < objmesh.shapes.size(); i++)
	{
		TriangleMesh* mesh = new TriangleMesh(objmesh.attrib, objmesh.shapes[i], Material(vec3(1, 1, 1), LIGHT), vec3(0, 2, 2));
		objects.push_back(std::unique_ptr<Object>(mesh));

		lights.push_back(std::unique_ptr<Light>(new MeshLight(mesh, 1, 5)));

		numtri += mesh->numfaces;

		clock_t start, end;
		start = clock();

		BVH* bvh = new BVH();
		bvh->ConstructBVH(mesh);
		bvh->objidx = objects.size() - 1;
		bvhs.push_back(std::unique_ptr<BVH>(bvh));

		end = clock();

		std::cout << "BVH Constructed: " << (double)(end - start) / CLOCKS_PER_SEC << " seconds" << std::endl;
	}

	// -----------------------------------------------------------
	// Spheres
	// --- Define center position and radius
	// --- Create Sphere object using position, radius, and any Material definition
	// -----------------------------------------------------------
	//vec3 posc1(-4, 0, -14);
	//vec3 posc2(3, -0.5, -8);
	//float rad1 = 2;
	//float rad2 = 1.5;
	//objects.push_back(std::unique_ptr<Object>(new Sphere(posc1, rad1, Material(vec3(1, 0, 1), DIFFUSE))));
	//objects.push_back(std::unique_ptr<Object>(new Sphere(posc2, rad2, Material(vec3(0.98, 0.98, 0.98), DIELECTRIC))));
	
	// -----------------------------------------------------------
	// Planes
	// --- Define "center" position and normal
	// --- Create Plane object using position, normal, and any Material definition
	// --- WARNING: Enabling this current plane will cover plane.obj above
	// -----------------------------------------------------------
	//vec3 posp(0, -1, -10);
	//vec3 norm(0, 1, 0);
	//objects.push_back(std::unique_ptr<Object>(new Plane(posp, norm, Material(vec3(1, 0, 0), DIFFUSE))));

	// -----------------------------------------------------------
	// Lights
	// --- Create any PointLight or DistantLight (or other lights defined as children of Light)
	// --- For PointLight, define position, colour, and intensity (can also define decay rate if PointLight is not infinite over distance, default decay is 0)
	// --- For DistantLight, define direction vector, colour, and intensity (no decay rate, but can be added similarly to PointLight if needed)
	// -----------------------------------------------------------
	//lights.push_back(std::unique_ptr<Light>(new DistantLight(vec3(0, -1, 0), 1, 1)));
	//lights.push_back(std::unique_ptr<Light>(new PointLight(vec3(-20, 70, 20), 1, 1)));
	//lights.push_back(std::unique_ptr<Light>(new PointLight(vec3(30, 50, 0), 1, 0.8)));

	clock_t start, end;
	start = clock();

	renderer.Render(camera, objects, lights, bvhs, curr);

	end = clock();

	std::cout << "Initial Render: " << (double)(end - start) / CLOCKS_PER_SEC << " seconds" << std::endl;
}

void Game::AddObject(const char* path, Material mat, vec3 offset)
{
	ObjMesh tmpmesh;

	renderer.LoadObjFile(path, NULL, true, tmpmesh);

	for (int i = 0; i < tmpmesh.shapes.size(); i++)
	{
		scene_attribs.push_back(tmpmesh.attrib);
		scene_shapes.push_back(tmpmesh.shapes[i]);
		scene_mats.push_back(mat);
		scene_offsets.push_back(offset);
	}
}

void Game::ParseScene()
{
	TriangleMesh* mesh = new TriangleMesh(scene_attribs, scene_shapes, scene_mats, scene_offsets);
	objects.push_back(std::unique_ptr<Object>(mesh));

	numtri += mesh->numfaces;

	clock_t start, end;
	start = clock();

	BVH* bvh = new BVH();
	bvh->ConstructBVH(mesh);
	bvh->objidx = objects.size() - 1;
	bvhs.push_back(std::unique_ptr<BVH>(bvh));

	end = clock();

	std::cout << "BVH Constructed: " << (double)(end - start) / CLOCKS_PER_SEC << " seconds" << std::endl;
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

bool Game::HandleInput()
{
	bool inputhandled = false;

	if (GetAsyncKeyState(0x41)) { camera.pos.x -= STEP_SIZE_POS; inputhandled = true; } //move camera left on x
	if (GetAsyncKeyState(0x44)) { camera.pos.x += STEP_SIZE_POS; inputhandled = true; } //move camera right on x
	if (GetAsyncKeyState(VK_SPACE)) { camera.pos.y += STEP_SIZE_POS; inputhandled = true; } //move camera up on y 
	if (GetAsyncKeyState(VK_LSHIFT)) { camera.pos.y -= STEP_SIZE_POS; inputhandled = true; } //move camera down on y
	if (GetAsyncKeyState(0x57)) { camera.pos.z -= STEP_SIZE_POS; inputhandled = true; } //move camera forward on z
	if (GetAsyncKeyState(0x53)) { camera.pos.z += STEP_SIZE_POS; inputhandled = true; } //move camera backward on z

	if (GetAsyncKeyState(VK_UP)) { camera.orient.x += STEP_SIZE_ORI;  inputhandled = true; } //tilt camera up around x //up arrow
	if (GetAsyncKeyState(VK_DOWN)) { camera.orient.x -= STEP_SIZE_ORI; inputhandled = true; } //tilt camera down around x //down arrow
	if (GetAsyncKeyState(VK_RIGHT)) { camera.orient.y -= STEP_SIZE_ORI; inputhandled = true; } //tilt camera right around y //right arrow
	if (GetAsyncKeyState(VK_LEFT)) { camera.orient.y += STEP_SIZE_ORI; inputhandled = true; } //tilt camera left around y //left arrow
	if (GetAsyncKeyState(0x45)) { camera.orient.z += STEP_SIZE_ORI; inputhandled = true; } //tilt camera right around z //E
	if (GetAsyncKeyState(0x51)) { camera.orient.z -= STEP_SIZE_ORI; inputhandled = true; } //tilt camera left around z //Q

	if (GetAsyncKeyState(0x43)) { camera.fov += STEP_SIZE_FOV; inputhandled = true; } //increase fov //C
	if (GetAsyncKeyState(0x56)) { camera.fov -= STEP_SIZE_FOV; inputhandled = true; } //decrease fov //V

	return inputhandled;
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void Game::Tick( float deltaTime )
{
	// clear the graphics window
	screen->Clear( 0 );

	// re-render only if camera has moved
	if (HandleInput()) { /*memset(renderer.buffer, 0, SCRWIDTH * SCRHEIGHT * sizeof(Color));*/ renderer.firstflag = true; }
	/*if (HandleInput())*/ renderer.Render(camera, objects, lights, bvhs, curr);

	curr->CopyTo(screen, 0, 0);

#ifdef DEBUG_INFO
	char printposx[32];		strcpy(printposx, ("pos.x: " + to_string(camera.pos.x)).c_str());
	char printposy[32];		strcpy(printposy, ("pos.y: " + to_string(camera.pos.y)).c_str());
	char printposz[32];		strcpy(printposz, ("pos.z: " + to_string(camera.pos.z)).c_str());
	char printorientx[32];	strcpy(printorientx, ("orient.x: " + to_string(camera.orient.x)).c_str());
	char printorienty[32];	strcpy(printorienty, ("orient.y: " + to_string(camera.orient.y)).c_str());
	char printorientz[32];	strcpy(printorientz, ("orient.z: " + to_string(camera.orient.z)).c_str());
	char printfov[32];		strcpy(printfov, ("fov: " + to_string(camera.fov)).c_str());

	char printnumtri[32];	strcpy(printnumtri, ("triangles: " + to_string(numtri)).c_str());

	screen->Print(printposx, 2, 2, 0xffffff);
	screen->Print(printposy, 2, 8, 0xffffff);
	screen->Print(printposz, 2, 14, 0xffffff);
	screen->Print(printorientx, 2, 20, 0xffffff);
	screen->Print(printorienty, 2, 26, 0xffffff);
	screen->Print(printorientz, 2, 32, 0xffffff);
	screen->Print(printfov, 2, 38, 0xffffff);

	screen->Print(printnumtri, 2, 50, 0xffffff);
#endif

}