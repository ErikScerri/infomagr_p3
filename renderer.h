#pragma once
#include "tiny_obj_loader.h"

namespace Tmpl8 {

#define MAX_DEPTH 10
#define TINYEPS 1e-8
#define SHADOWBIAS 0.0001
#define LEAFCOUNT 3
#define BINSPACING 2

#define BINNEDSAH

typedef vec3 Color;
const Color BLACK = Color(0, 0, 0);
const Color WHITE = Color(1, 1, 1);
const Color RED = Color(1, 0, 0);
const Color GREEN = Color(0, 1, 0);
const Color BLUE = Color(0, 0, 1);

struct Triangle
{
	vec3 v0, v1, v2;
	float area;
	vec3 Centroid() const { return (v0 + v1 + v2) * (1.0f / 3.0f); }
	void SetArea() { vec3 e3 = cross(v1 - v0, v2 - v0); area = 0.5 * e3.length(); }
};
struct ObjMesh { tinyobj::attrib_t attrib; std::vector<tinyobj::shape_t> shapes; std::vector<tinyobj::material_t> materials; };

static vec3 transformpoint(const vec3& src, const mat4& M)
{
	float a, b, c, w;
	vec3 dst;

	a = src.cell[0] * M.cell[0] + src.cell[1] * M.cell[1] + src.cell[2] * M.cell[2] + M.cell[3];
	b = src.cell[0] * M.cell[4] + src.cell[1] * M.cell[5] + src.cell[2] * M.cell[6] + M.cell[7];
	c = src.cell[0] * M.cell[8] + src.cell[1] * M.cell[9] + src.cell[2] * M.cell[10] + M.cell[11];
	w = src.cell[0] * M.cell[12] + src.cell[1] * M.cell[13] + src.cell[2] * M.cell[14] + M.cell[15];

	dst.x = a / w;
	dst.y = b / w;
	dst.z = c / w;

	return dst;
}

static vec3 transformvector(const vec3& src, const mat4& M)
{
	float a, b, c;
	vec3 dst;

	a = src[0] * M.cell[0] + src[1] * M.cell[1] + src[2] * M.cell[2];
	b = src[0] * M.cell[4] + src[1] * M.cell[5] + src[2] * M.cell[6];
	c = src[0] * M.cell[8] + src[1] * M.cell[9] + src[2] * M.cell[10];

	dst.x = a;
	dst.y = b;
	dst.z = c;
	
	return dst;
}

static vec3 crossvectors(const vec3& vector1, const vec3& vector2)
{
	float a, b, c;

	a = vector1.y * vector2.z - vector1.z * vector2.y;
	b = vector1.z * vector2.x - vector1.x * vector2.z;
	c = vector1.x * vector2.y - vector1.y * vector2.x;

	return vec3(a, b, c);
}

static bool solvequad(const float &a, const float &b, const float &c, float &x0, float &x1)
{
	float discriminant = (b * b) - (4 * a * c);

	if (discriminant < 0) return false;
	else if (discriminant == 0) x0 = x1 = -0.5 * b / a;
	else
	{
		float q = (b > 0) ? -0.5 * (b + sqrtf(discriminant)) : -0.5 * (b - sqrtf(discriminant));
		x0 = q / a;
		x1 = c / q;
	}
	if (x0 > x1) std::swap(x0, x1);

	return true;
}

inline vec3 mix(const vec3 &a, const vec3& b, const float &mixValue)
{
	return a * (1 - mixValue) + b * mixValue;
}

// -----------------------------------------------------------
// Camera & Rays
// -----------------------------------------------------------
class Camera
{
public:
	Camera() : pos(0), orient(0), w(SCRWIDTH), h(SCRHEIGHT), fov(90) {}
	Camera(const float &f) : pos(0), orient(0), w(SCRWIDTH), h(SCRHEIGHT), fov(f) {}
	Camera(const float &f, const vec3 &p, const vec3 &o) : pos(p), orient(o), w(SCRWIDTH), h(SCRHEIGHT), fov(f) {}
	Camera(const float &f, const vec3 &p, const vec3 &o, const uint32_t &wid, const uint32_t &hei) : pos(p), orient(o), w(wid), h(hei), fov(f) {}

	vec3 pos, orient;
	float fov;
	uint32_t w, h;
	//Color background = Color(0.235294, 0.67451, 0.843137);
	Color background = Color(0.15, 0.15, 0.15);
	//Color background = BLACK;
};

enum RayType { PRIMARY, SHADOW };

class Ray
{
public:
	Ray() : orig(0), dir(0, 0, -1), t(FLT_MAX), type(PRIMARY) {}
	Ray(const vec3 &o, const vec3 &d, const RayType &ty = PRIMARY) : orig(o), dir(d), t(FLT_MAX), type(ty) {}

	vec3 orig; // origin vector
	vec3 dir; // direction vector
	float t; // minimum distance to intersection
	// any point on ray can be defined as P = orig + t * dir
	RayType type;
};

// -----------------------------------------------------------
// Materials
// -----------------------------------------------------------
enum MaterialType { DIFFUSE, MIRROR, DIELECTRIC, LIGHT };

class Material
{
public:
	Material() {};
	Material(const vec3 &c, const MaterialType &mt) : color(c), type(mt), idxofref(1.5), ratioD(0.8), ratioS(0.2), specExp(10), albedo(1) {}
	Material(const vec3 &c, const MaterialType &mt, const float &ior, const float &rD = 0.8, const float &rS = 0.2, const float &sE = 10, const float &alb = 1) :
		color(c), type(mt), idxofref(ior), ratioD(rD), ratioS(rS), specExp(sE), albedo(alb) {};
	Color color;
	MaterialType type;
	float ratioD, ratioS, specExp, idxofref, albedo;
};

// -----------------------------------------------------------
// Object Definitions
// -----------------------------------------------------------
class Object
{
public:
	Object() {}
	virtual ~Object() {}

	virtual bool Intersect(Ray &ray, uint32_t &idx) const = 0;
	virtual bool Intersect(Ray &ray, uint32_t &idx, const int &first, const int &count) const { return false; };
	virtual void GetSurfaceData(const vec3 &hitPoint, vec3 &hitNorm, uint32_t collidx = NULL) const = 0;

	virtual Material GetMaterial(uint32_t i) const { return mat; }
protected:
	Material mat;
};

class Plane : public Object
{
public:
	Plane(const vec3 &p, const vec3 &n, Material m) : pos(p), norm(n) { mat = m; }
	~Plane() {}

	bool Intersect(Ray &ray, uint32_t &idx) const
	{
		float dotln = dot(norm, ray.dir);
		if (abs(dotln) > TINYEPS)
		{
			vec3 posorig = pos - ray.orig;
			float tmp = dot(posorig, norm) / dotln;
			ray.t = tmp >= 0 ? tmp : ray.t;
			return (tmp >= 0);
		}

		return false;
	}

	void GetSurfaceData(const vec3 &hitPoint, vec3 &hitNorm, uint32_t collidx = NULL) const
	{
		hitNorm = norm;
	}
private:
	vec3 pos, norm;
};

class Sphere : public Object
{
public:
	Sphere(const vec3 &p, const float &r, Material m) : pos(p), r(r), r2(r * r) { mat = m; }
	~Sphere() {}

	bool Intersect(Ray &ray, uint32_t &idx) const
	{
		vec3 c = pos - ray.orig;
		float t = dot(c, ray.dir);
		vec3 q = c - t * ray.dir;

		float p2 = dot(q, q);

		if (p2 > r2) return false;

		t -= sqrtf(r2 - p2);
		if ((t < ray.t) && (t > 0)) ray.t = t;
		return true;
	}

	void GetSurfaceData(const vec3 &hitPoint, vec3 &hitNorm, uint32_t collidx = NULL) const
	{
		hitNorm = (hitPoint - pos).normalized();
	}
private:
	float r, r2;
	vec3 pos;
};

class TriangleMesh : public Object
{
public:
	TriangleMesh(tinyobj::attrib_t attrib, tinyobj::shape_t shape, Material m, const vec3 &offset = 0)
	{
		numfaces = shape.mesh.num_face_vertices.size();

		faces = (Triangle*)MALLOC64(numfaces * sizeof(Triangle), 64);
		normals = (vec3*)MALLOC64(numfaces * sizeof(vec3), 64);
		mats = (Material*)MALLOC64(numfaces * sizeof(Material), 64);

		uint32_t idxstart = 0;
		for (int i = 0; i < numfaces; i++)
		{
			uint32_t numvtx = shape.mesh.num_face_vertices[i];
			if (numvtx != 3) { idxstart += numvtx; continue; }

			tinyobj::index_t vtx0 = shape.mesh.indices[idxstart];
			tinyobj::index_t vtx1 = shape.mesh.indices[idxstart + 1];
			tinyobj::index_t vtx2 = shape.mesh.indices[idxstart + 2];

			uint32_t vtx0idx = vtx0.vertex_index * 3;
			faces[i].v0 = vec3(attrib.vertices[vtx0idx], attrib.vertices[vtx0idx + 1], attrib.vertices[vtx0idx + 2]) + offset;
			uint32_t vtx1idx = vtx1.vertex_index * 3;
			faces[i].v1 = vec3(attrib.vertices[vtx1idx], attrib.vertices[vtx1idx + 1], attrib.vertices[vtx1idx + 2]) + offset;
			uint32_t vtx2idx = vtx2.vertex_index * 3;
			faces[i].v2 = vec3(attrib.vertices[vtx2idx], attrib.vertices[vtx2idx + 1], attrib.vertices[vtx2idx + 2]) + offset;

			uint32_t normidx = vtx0.normal_index * 3;
			if (attrib.normals.size() > 0)
				normals[i] = vec3(attrib.normals[normidx], attrib.normals[normidx + 1], attrib.normals[normidx + 2]);
			else
				normals[i] = cross(faces[i].v1 - faces[i].v0 , faces[i].v2 - faces[i].v0).normalized();

			// Surface Area of each face for SAH
			faces[i].SetArea();

			mats[i] = m;

			idxstart += 3;
		}
	}
	TriangleMesh(std::vector<tinyobj::attrib_t> attribs, std::vector<tinyobj::shape_t> shapes, std::vector<Material> m, std::vector<vec3> offsets)
	{
		numfaces = 0;
		for (int s = 0; s < shapes.size(); s++) numfaces += shapes[s].mesh.num_face_vertices.size();

		faces = (Triangle*)MALLOC64(numfaces * sizeof(Triangle), 64);
		normals = (vec3*)MALLOC64(numfaces * sizeof(vec3), 64);
		mats = (Material*)MALLOC64(numfaces * sizeof(Material), 64);

		uint32_t faceidx = 0;

		for (int s = 0; s < shapes.size(); s++)
		{
			uint32_t idxstart = 0;
			for (int i = 0; i < shapes[s].mesh.num_face_vertices.size(); i++)
			{
				uint32_t numvtx = shapes[s].mesh.num_face_vertices[i];
				if (numvtx != 3) { idxstart += numvtx; continue; }

				tinyobj::index_t vtx0 = shapes[s].mesh.indices[idxstart];
				tinyobj::index_t vtx1 = shapes[s].mesh.indices[idxstart + 1];
				tinyobj::index_t vtx2 = shapes[s].mesh.indices[idxstart + 2];

				uint32_t vtx0idx = vtx0.vertex_index * 3;
				faces[faceidx].v0 = vec3(attribs[s].vertices[vtx0idx], attribs[s].vertices[vtx0idx + 1], attribs[s].vertices[vtx0idx + 2]) + offsets[s];
				uint32_t vtx1idx = vtx1.vertex_index * 3;
				faces[faceidx].v1 = vec3(attribs[s].vertices[vtx1idx], attribs[s].vertices[vtx1idx + 1], attribs[s].vertices[vtx1idx + 2]) + offsets[s];
				uint32_t vtx2idx = vtx2.vertex_index * 3;
				faces[faceidx].v2 = vec3(attribs[s].vertices[vtx2idx], attribs[s].vertices[vtx2idx + 1], attribs[s].vertices[vtx2idx + 2]) + offsets[s];

				uint32_t normidx = vtx0.normal_index * 3;
				if (attribs[s].normals.size() > 0)
					normals[faceidx] = vec3(attribs[s].normals[normidx], attribs[s].normals[normidx + 1], attribs[s].normals[normidx + 2]);
				else
					normals[faceidx] = cross(faces[faceidx].v1 - faces[faceidx].v0, faces[faceidx].v2 - faces[faceidx].v0).normalized();

				faces[faceidx].SetArea();

				mats[faceidx] = m[s];

				idxstart += 3;
				faceidx++;
			}
		}
	}

	~TriangleMesh() {}

	bool Intersect(Ray &ray, uint32_t &idx) const
	{
		bool result = false;
		float dist = INFINITY, u, v;
		for (int i = 0; i < numfaces; i++)
		{
			Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
			if (IntersectTriangle(rayTMP, faces[i], u, v) && rayTMP.t < ray.t)
			{
				ray.t = rayTMP.t;
				idx = i;
				result = true;
			}
		}

		return result;
	}

	bool Intersect(Ray &ray, uint32_t &idx, const int &first, const int &count) const
	{
		bool result = false;
		float dist = INFINITY, u, v;
		for (int i = first; i < first + count; i++)
		{
			Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
			if (IntersectTriangle(rayTMP, faces[i], u, v) && rayTMP.t < ray.t)
			{
				ray.t = rayTMP.t;
				idx = i;
				result = true;
			}
		}

		return result;
	}

	void GetSurfaceData(const vec3 &hitPoint, vec3 &hitNorm, uint32_t collidx = NULL) const
	{
		hitNorm = normals[collidx];
	}

	void SetMaterial(uint32_t i, Material m) const
	{
		mats[i] = m;
	}

	Material GetMaterial(uint32_t i) const
	{
		return mats[i];
	}
	
	uint32_t numfaces;
	Triangle* faces;
	vec3* normals;
	Material* mats;
private:
	// M�ller-Trumbore algorithm
	bool IntersectTriangle(Ray &ray, Triangle &tri, float &u, float &v) const
	{
		vec3 v0v1 = tri.v1 - tri.v0;
		vec3 v0v2 = tri.v2 - tri.v0;
		vec3 vecp = cross(ray.dir, v0v2);
		float det = dot(v0v1, vecp);

		if (abs(det) < TINYEPS) return false;

		float detinv = 1 / det;

		vec3 vect = ray.orig - tri.v0;
		u = dot(vect, vecp) * detinv;
		if (u < 0 || u > 1) return false;

		vec3 vecq = cross(vect, v0v1);
		v = dot(ray.dir, vecq) * detinv;
		if (v < 0 || u + v > 1) return false;

		ray.t = dot(v0v2, vecq) * detinv;

		return (ray.t > 0) ? true : false;
	}
};

// -----------------------------------------------------------
// Lights
// -----------------------------------------------------------
class Light
{
public:
	Light(const vec3 &p, const vec3 &d, const Color &c, const float &i, const float &dec) : pos(p), dir(d), color(c), intensity(i), decay(dec) {}
	virtual ~Light() {}

	virtual void Illuminate(const vec3 &hitPoint, vec3 &lightDir, Color &lightColorInt, float &dist) const = 0;
	virtual void RandomPointOnLight(vec3 &lightOrig, vec3 &lightNorm, float &lightArea, std::ranlux48 gen) const { lightOrig = pos; lightNorm = dir; lightArea = 0; };

	vec3 pos, dir;
	float intensity, decay;
	Color color;

	//std::default_random_engine generator;
};

class DistantLight : public Light
{
public:
	DistantLight(const vec3 &d, const Color &c = 1, const float &i = 1) : Light(0, d, c, i, 0) {}
	~DistantLight() {}

	void Illuminate(const vec3 &hitPoint, vec3 &lightDir, Color &lightColorInt, float &dist) const
	{
		lightDir = -dir;
		dist = FLT_MAX;
		lightColorInt = color * intensity;
	}
};

class PointLight : public Light
{
public:
	PointLight(const vec3 &p, const Color &c = 1, const float &i = 1, const float &dec = 0) : Light(p, 0, c, i, dec) {}
	~PointLight() {}

	void Illuminate(const vec3 &hitPoint, vec3 &lightDir, Color &lightColorInt, float &dist) const
	{
		lightDir = (hitPoint - pos);
		float r2 = lightDir.sqrLength();
		dist = sqrt(r2);
		lightDir.x /= dist;
		lightDir.y /= dist;
		lightDir.z /= dist;

		lightColorInt = decay == 0 ? color * intensity : color * (intensity / (decay * PI * r2));
	}
};

class MeshLight : public Light
{
public:
	MeshLight(TriangleMesh* m, const Color &c = 1, const float &i = 1, const float &dec = 0) : Light(0, 0, c, i, dec), mesh(m)
	{
		area = 0;
		for (int i = 0; i < mesh->numfaces; i++) { area += mesh->faces[i].area; }
	}
	~MeshLight() {}

	void Illuminate(const vec3 &hitPoint, vec3 &lightDir, Color &lightColorInt, float &dist) const
	{
		lightColorInt = color * intensity;
	}

	void RandomPointOnLight(vec3 &lightOrig, vec3 &lightNorm, float &lightArea, std::ranlux48 gen) const
	{
		int randFace = rand() % mesh->numfaces;

		std::uniform_real_distribution<float> distribution(0, 1);

		//3D Triangle Point Pick Formula
		float r1 = distribution(gen);
		float r2 = distribution(gen);

		r1 = r1 >= 1 ? 1 - r1 : r1;
		r2 = r2 >= 1 ? 1 - r2 : r2;

		vec3 v0v1 = mesh->faces[randFace].v1 - mesh->faces[randFace].v0;
		vec3 v0v2 = mesh->faces[randFace].v2 - mesh->faces[randFace].v0;

		lightOrig = mesh->faces[randFace].v0 + r1 * (v0v1) + r2 * (v0v2);
		lightNorm = mesh->normals[randFace];
		lightArea = area;
	}
	
	TriangleMesh* mesh;
	float area;
};

// -----------------------------------------------------------
// Classes for BVH
// -----------------------------------------------------------
class vec3f
{
public:
	union { struct { float x, y, z; }; float cell[3]; };
	vec3f() {}
	vec3f(float v) : x(v), y(v), z(v) {}
	vec3f(float x, float y, float z) : x(x), y(y), z(z) {}
	vec3f operator - () const { return vec3f(-x, -y, -z); }
	vec3f operator + (const vec3f& addOperand) const { return vec3f(x + addOperand.x, y + addOperand.y, z + addOperand.z); }
	vec3f operator - (const vec3f& operand) const { return vec3f(x - operand.x, y - operand.y, z - operand.z); }
	vec3f operator * (const vec3f& operand) const { return vec3f(x * operand.x, y * operand.y, z * operand.z); }
	void operator -= (const vec3f& a) { x -= a.x; y -= a.y; z -= a.z; }
	void operator += (const vec3f& a) { x += a.x; y += a.y; z += a.z; }
	void operator *= (const vec3f& a) { x *= a.x; y *= a.y; z *= a.z; }
	void operator *= (const float a) { x *= a; y *= a; z *= a; }
	float operator [] (const uint& idx) const { return cell[idx]; }
	float& operator [] (const uint& idx) { return cell[idx]; }
	vec3 tovec3() const { return vec3(x, y, z); }
};

class BoundingBox
{
public:
	BoundingBox() {}
	BoundingBox(const vec3f &min_, const vec3f &max_)
	{
		bounds[0] = min_;
		bounds[1] = max_;
	}
	~BoundingBox() {}

	bool Intersect(Ray &ray, int* sign) const
	{
		float tmin, tmax, tymin, tymax, tzmin, tzmax;

		tmin = (bounds[sign[0]].x - ray.orig.x) * ray.dir.x;
		tmax = (bounds[1 - sign[0]].x - ray.orig.x) * ray.dir.x;
		tymin = (bounds[sign[1]].y - ray.orig.y) * ray.dir.y;
		tymax = (bounds[1 - sign[1]].y - ray.orig.y) * ray.dir.y;

		if ((tmin > tymax) || (tymin > tmax)) return false;
		if (tymin > tmin) tmin = tymin;
		if (tymax < tmax) tmax = tymax;

		tzmin = (bounds[sign[2]].z - ray.orig.z) * ray.dir.z;
		tzmax = (bounds[1 - sign[2]].z - ray.orig.z) * ray.dir.z;

		if ((tmin > tzmax) || (tzmin > tmax)) return false;
		if (tzmin > tmin) tmin = tzmin;
		if (tzmax < tmax) tmax = tzmax;

		ray.t = tmin;

		return true;
	}

	uint GetLongestAxis() const
	{
		float x = abs(bounds[0].x - bounds[1].x);
		float y = abs(bounds[0].y - bounds[1].y);
		float z = abs(bounds[0].z - bounds[1].z);

		uint axis = 0;
		float max = x;

		if (y > max) { axis = 1; max = y; }
		if (z > max) { axis = 2; max = z; }

		return axis;
	}

	BoundingBox& Extend(const vec3 &pt)
	{
		if (pt.x < bounds[0].x) bounds[0].x = pt.x;
		if (pt.y < bounds[0].y) bounds[0].y = pt.y;
		if (pt.z < bounds[0].z) bounds[0].z = pt.z;
		if (pt.x > bounds[1].x) bounds[1].x = pt.x;
		if (pt.y > bounds[1].y) bounds[1].y = pt.y;
		if (pt.z > bounds[1].z) bounds[1].z = pt.z;

		return *this;
	}

	BoundingBox& Extend(const Triangle &tr)
	{
		if (tr.v0.x < bounds[0].x) bounds[0].x = tr.v0.x;
		if (tr.v0.y < bounds[0].y) bounds[0].y = tr.v0.y;
		if (tr.v0.z < bounds[0].z) bounds[0].z = tr.v0.z;
		if (tr.v0.x > bounds[1].x) bounds[1].x = tr.v0.x;
		if (tr.v0.y > bounds[1].y) bounds[1].y = tr.v0.y;
		if (tr.v0.z > bounds[1].z) bounds[1].z = tr.v0.z;

		if (tr.v1.x < bounds[0].x) bounds[0].x = tr.v1.x;
		if (tr.v1.y < bounds[0].y) bounds[0].y = tr.v1.y;
		if (tr.v1.z < bounds[0].z) bounds[0].z = tr.v1.z;
		if (tr.v1.x > bounds[1].x) bounds[1].x = tr.v1.x;
		if (tr.v1.y > bounds[1].y) bounds[1].y = tr.v1.y;
		if (tr.v1.z > bounds[1].z) bounds[1].z = tr.v1.z;

		if (tr.v2.x < bounds[0].x) bounds[0].x = tr.v2.x;
		if (tr.v2.y < bounds[0].y) bounds[0].y = tr.v2.y;
		if (tr.v2.z < bounds[0].z) bounds[0].z = tr.v2.z;
		if (tr.v2.x > bounds[1].x) bounds[1].x = tr.v2.x;
		if (tr.v2.y > bounds[1].y) bounds[1].y = tr.v2.y;
		if (tr.v2.z > bounds[1].z) bounds[1].z = tr.v2.z;

		return *this;
	}

	vec3f Centroid() const { return (bounds[0] + bounds[1]) * 0.5; }

	vec3f bounds[2] = { FLT_MAX, -FLT_MAX };
};

struct BVHNode
{
//Minimal 32-byte size
public:
	BoundingBox bbox;
	int left_first, count;
};

class BVH
{
public:
	BVH() {}
	~BVH() {}

	uint objidx, N;
	BVHNode root;
	
	BVHNode* pool;

	void ConstructBVH(TriangleMesh* &mesh)
	{
		N = mesh->numfaces;

		centroids = new vec3[N];
		for (int i = 0; i < N; i++) centroids[i] = mesh->faces[i].Centroid();

		indices = new uint[N];
		for (int i = 0; i < N; i++) indices[i] = i;		

		//Align 32-byte nodes to cachelines for optimal caching performance
		pool = (BVHNode*)_aligned_malloc((N * 2 - 1) * sizeof(BVHNode), 64);
		root = pool[0];
		poolPtr = 2;

		root.count = N;
		root.left_first = 0;
		root.bbox = CalculateBounds(mesh->faces, 0, N);

		Subdivide(root, mesh);
		pool[0] = root;
	}

	void Subdivide(BVHNode &node, TriangleMesh* &mesh)
	{
		if (node.count < LEAFCOUNT) return;

#ifdef BINNEDSAH
		float mincost = FLT_MAX;
		int start = node.left_first;
		int split;

		// Calculate surface area of parent node
		float area_parent = CalculateSurfaceArea(mesh->faces, node.left_first, node.count);

		// Go through splits at discrete intervals determined by BINSPACING constant
		for (int i = start; i < start + node.count; i += BINSPACING)
		{
			int count_left = i - node.left_first;
			int count_right = node.count - count_left;

			float area_left = CalculateSurfaceArea(mesh->faces, node.left_first, count_left);
			float area_right = CalculateSurfaceArea(mesh->faces, i, count_right);

			// SURFACE AREA HEURISTIC
			float cost = ((area_left / area_parent) * count_left) + ((area_right / area_parent) * count_right);

			// Determine split point where cost is minimal
			if (cost < mincost) { mincost = cost; split = i; }
		}

		// If not splitting is the best option, return
		if (split == start) return;
#else
		//Alternative to SAH: Split at midpoint of array
		int split = node.left_first + (node.count / 2);
#endif // BINNEDSAH		
				
		pool[poolPtr].count = split - node.left_first;
		pool[poolPtr].left_first = node.left_first;
		pool[poolPtr].bbox = CalculateBounds(mesh->faces, pool[poolPtr].left_first, pool[poolPtr].count);

		pool[poolPtr + 1].count = node.count - (split - node.left_first);
		pool[poolPtr + 1].left_first = split;
		pool[poolPtr + 1].bbox = CalculateBounds(mesh->faces, pool[poolPtr + 1].left_first, pool[poolPtr + 1].count);

		node.left_first = poolPtr;
		poolPtr += 2;

		Subdivide(pool[node.left_first], mesh);
		Subdivide(pool[node.left_first + 1], mesh);
	}

	//First and Count determine which range of the primitives array to cater the AABB for
	BoundingBox CalculateBounds(const Triangle* triangles, const int &first, const int &count)
	{
		BoundingBox bounds = BoundingBox();

		for (int i = first; i < first + count; i++)
		{
			uint idx = indices[i];
			bounds.Extend(triangles[i]);
		}

		return bounds;
	}

	float CalculateSurfaceArea(const Triangle* triangles, const int &first, const int &count)
	{
		float area = 0;
		
		for (int i = first; i < first + count; i++)
		{
			uint idx = indices[i];
			area += triangles[i].area;
		}

		return area;
	}

	void QuickSort(uint arr[], vec3 cent[], uint left, uint right, uint axis)
	{
		int i = left, j = right;
		uint tmp;
		vec3 tmpv;
		float pivot = cent[(left + right) / 2][axis];

		/* partition */
		while (i <= j)
		{
			while (cent[i][axis] < pivot)
				i++;
			while (cent[j][axis] > pivot)
				j--;
			if (i <= j) {
				tmpv = cent[i];
				cent[i] = cent[j];
				cent[j] = tmpv;

				tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;

				i++;
				j--;
			}
		};

		/* recursion */
		if (left < j)
			QuickSort(arr, cent, left, j, axis);
		if (i < right)
			QuickSort(arr, cent, i, right, axis);
	}
private:
	uint* indices;
	vec3* centroids;
	uint poolPtr;
};

// -----------------------------------------------------------
// Actual Render Framework
// -----------------------------------------------------------
class Renderer
{
public:
	Renderer() {}
	mat4 SetCameraMatrix(const Camera &camera);
	void Render(const Camera &camera, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, Surface* screen);
	Color Sample(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, const Camera &camera, uint32_t depth, const bool lastSpecular);
	bool NearestIntersection(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const Object* &hitObj, uint32_t &collidx);
	bool NearestIntersection(Ray &ray, Object* &hitObj, const int &first, const int &count, uint32_t &collidx);
	bool TraverseBVH(Ray &ray, Ray &invray, int* sign, BVH* bvh, BVHNode &node, Object* &hitObj, uint32_t &collidx);
	bool LoadObjFile(const char* filename, const char* basepath, bool triangulate, ObjMesh &obj);

	void pp_BOX_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height, const unsigned int radius);
	void pp_BOX_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height, const unsigned int radius);
	void pp_BOX_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height, const unsigned int radius);

	void pp_BLUR_LIGHT_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height);
	void pp_BLUR_LIGHT_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height);
	void pp_BLUR_LIGHT_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height);

	void pp_BLUR_HEAVY_FILTER_x(const Color *src, Color *dst, const unsigned int width, const unsigned int height);
	void pp_BLUR_HEAVY_FILTER_y(const Color *src, Color *dst, const unsigned int width, const unsigned int height);
	void pp_BLUR_HEAVY_FILTER(Color *frameBuffer, Color *tmpFrameBuffer, const unsigned int width, const unsigned int height);

	Color* buffer = (Color*)MALLOC64(SCRWIDTH * SCRHEIGHT * sizeof(Color), 64);
	bool firstflag = true;
private:
	//std::default_random_engine generator;
	std::ranlux48 generator;

	vec3 Reflect(const vec3 &I, const vec3 &N)
	{
		return I - (2 * dot(I, N) * N);
	}
	vec3 Refract(const vec3 &I, const vec3 &N, const float &idxofref)
	{
		float cosI = CLAMP(dot(I, N), -1, 1);
		float etaI = 1, etaT = idxofref;

		vec3 n = N;
		if (cosI < 0) { cosI = -cosI; } else { std::swap(etaI, etaT); n = -N; }

		float eta = etaI / etaT;
		float k = 1 - eta * eta * (1 - cosI * cosI);
		return k < 0 ? 0 : eta * I + (eta * cosI - sqrtf(k)) * n;
	}
	vec3 Diffuse(const vec3 &N, float &r1, float &r2)
	{		
		float sinTheta = sqrtf(1 - r1 * r1);
		float phi = 2 * PI * r2;
		float x = sinTheta * cosf(phi);
		float z = sinTheta * sinf(phi);
		vec3 dir = vec3(x, r1, z);

		vec3 Nt, Nb;
		if (std::fabs(N.x) > std::fabs(N.y))
			Nt = vec3(N.z, 0, -N.x) * (1 / sqrtf(N.x * N.x + N.z * N.z));
		else
			Nt = vec3(0, -N.z, N.y) * (1 / sqrtf(N.y * N.y + N.z * N.z));
		Nb = cross(N, Nt);

		return vec3(
			dir.x * Nb.x + dir.y * N.x + dir.z * Nt.x,
			dir.x * Nb.y + dir.y * N.y + dir.z * Nt.y,
			dir.x * Nb.z + dir.y * N.z + dir.z * Nt.z
		);
	}
	void Fresnel(const vec3 &I, const vec3 &N, const float &idxofref, float &kr)
	{
		float cosI = CLAMP(dot(I, N), -1, 1);
		float etaI = 1, etaT = idxofref;

		if (cosI > 0) { std::swap(etaI, etaT); }
		// Snell's law
		float sinT = etaI / etaT * sqrtf(MAX(0.f, 1 - cosI * cosI));
		// Total internal reflection
		if (sinT >= 1) {
			kr = 1;
		}
		else {
			float cosT = sqrtf(MAX(0.f, 1 - sinT * sinT));
			cosI = fabsf(cosI);
			float Rs = ((etaT * cosI) - (etaI * cosT)) / ((etaT * cosI) + (etaI * cosT));
			float Rp = ((etaI * cosI) - (etaT * cosT)) / ((etaI * cosI) + (etaT * cosT));
			kr = (Rs * Rs + Rp * Rp) / 2;
		}
	}
};

}; // namespace Tmpl8